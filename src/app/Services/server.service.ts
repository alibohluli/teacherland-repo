import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Config} from '@angular/fire/analytics';
import {Observable} from 'rxjs';

@Injectable()
export class ServerService{
    constructor(private http: HttpClient){}
    storeAdmins = (users: any[]) => {
       return this.http.post('https://angular-teacherland-default-rtdb.firebaseio.com/admins.json' , users);
    }
    storeUsers = (users: Config) => {
        return this.http.post('https://angular-teacherland-default-rtdb.firebaseio.com/users.json' , users);
    }
    getUser(): Observable<HttpResponse<Config>> {
        return this.http.get<Config>('https://angular-teacherland-default-rtdb.firebaseio.com/users.json', { observe: 'response' });
    }
    getNewUser(): any {
        return this.http.get('https://angular-teacherland-default-rtdb.firebaseio.com/users.json', { observe: 'response' });
    }
}
