import { Component, OnInit } from '@angular/core';
import {AlertService} from '../Services/alert.service';
import {ServerService} from '../Services/server.service';
import {HttpHeaders, HttpResponse} from '@angular/common/http';
import {from, Observable} from 'rxjs';

import { HttpClientModule } from '@angular/common/http';
import {Config} from '@angular/fire/analytics';



@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css'],
  providers: [AlertService],
})
export class LoginFormComponent implements OnInit {
  constructor(private alertService: AlertService , private serverService: ServerService) { }

    config: Config | undefined;
    // headers: HttpHeaders | {[header: string]: string | string[]};
    ngOnInit(): void {
    setTimeout(() => {
      this.alertService.showSomeMessage('shoma varede safhe ye login shodid !!'); }, 1000 );
  }
    onGetUser = () => {
    this.serverService.getUser().subscribe(
        (response: Config) => {
          console.log(response);
          // const data = response.json();
          // console.log(data);
        },
        (error) => console.log(error),
    );
  }
    onNewGet(): void {
        this.serverService.getUser().subscribe(
            (response) => {
                console.log(response);
                console.log(response.body);
                // Object.keys(response).forEach((key: any) => console.log(key + ':' + response[key]));
                for (const key in response.body) {
                    if (true){
                        console.log(key, response[key]);
                        console.log(`${key}: ${response[key]}`);
                    }
                }
            },
    );
    }

}
