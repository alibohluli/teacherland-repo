import { Component, OnInit } from '@angular/core';
import {ServerService} from '../Services/server.service';

@Component({
  selector: 'app-signup-form',
  templateUrl: './signup-form.component.html',
  styleUrls: ['./signup-form.component.css']
})
export class SignupFormComponent implements OnInit {

  constructor( private serverService: ServerService) { }

  ngOnInit(): void {
  }

  onSaveUsers = (user) => {
   // console.log(user);
   // console.log(user.form.value);
    this.serverService.storeUsers(user.form.value).subscribe(
        response => {
          alert('ثبت نام با موفقیت انجام شد');
          user.reset();
        },
        error => alert('ثبت نام با خطا مواجه شد، دوباره تلاش کنید')                   /*console.log(error)*/
    );
  }

}
