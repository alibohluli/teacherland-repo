import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-course-card',
  templateUrl: './course-card.component.html',
  styleUrls: ['./course-card.component.css']
})
export class CourseCardComponent{

    constructor() { }

    coursesInfo = [
        {title: 'اولین تیتر' , subject: 'پایتون' , teacher: 'علی بهلولی' , background: 'work-1.jpg' , price: '120.000' , students: '1000'},
        {title: 'secondTitle' , subject: 'python' , teacher: 'Ali Bohlooli' , background: 'work-2.jpg' , price: '20000' , students: '2000'},
        {title: 'thirdTitle' , subject: 'python' , teacher: 'Ali Bohlooli' , background: 'work-3.jpg' , price: '128000' , students: '400'},
        {title: '4thTitle' , subject: 'python' , teacher: 'Ali Bohlooli' , background: 'work-4.jpg' , price: '120000' , students: '1000'},
        {title: '5thTitle' , subject: 'python' , teacher: 'Ali Bohlooli' , background: 'work-5.jpg' , price: '120000' , students: '1000'},
        {title: '6thTitle' , subject: 'python' , teacher: 'Ali Bohlooli' , background: 'work-6.jpg' , price: '120000' , students: '1000'},
        ];

}
