import { Component } from '@angular/core';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.css']
})
export class CategoriesComponent {

  constructor() { }

  categoriess = [
    {course : 'کنکور' ,                  number : 27 , background : 'work-1.jpg'},
    {course : 'بازاریابی' ,             number : 42 , background : 'work-2.jpg'},
    {course : 'عکاسی' ,                 number : 11 , background : 'work-3.jpg'},
    {course : 'موسیقی' ,                number : 5 ,  background : 'work-9.jpg'},
    {course : 'نرم افزار و تکنولوژی' , number : 61 , background : 'work-5.jpg'},
    {course : 'ابتدایی' ,               number : 18 , background : 'work-6.jpg'},
  ];
}
