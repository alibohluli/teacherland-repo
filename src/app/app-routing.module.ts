import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {ContactUsComponent} from './contact-us/contact-us.component';
import {MainPageComponent} from './main-page/main-page.component';
import {CourseCardComponent} from './course-card/course-card.component';
import {LoginFormComponent} from './login-form/login-form.component';
import {SignupFormComponent} from './signup-form/signup-form.component';

const routes: Routes = [
  {path : '' , component: MainPageComponent},
  {path : 'contact-us' , component: ContactUsComponent },
  {path : 'card' , component: CourseCardComponent },
  {path : 'login' , component: LoginFormComponent },
  {path : 'sign-up' , component: SignupFormComponent },
  //  {path : '**' , component: NotFound},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
