import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';

import { Observable } from 'rxjs';

@Component({
  selector: 'contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.css']
})
export class ContactUsComponent {


  forms? :any[];
  

  personalInfo = [
    { Name : "نام و نام خانوادگی" , PlaceH : "علی علوی" },
    { Name : "پست الکترونیک" , PlaceH : "someone@gmail.com" },
    //{ Name : "موضوع" , PlaceH : "شکایت" },
  ];

  constructor(private db : AngularFireDatabase) { 

    db.list('/forms') // refer to firebase DB 
      .valueChanges().subscribe (dbForms =>{
        this.forms = dbForms;       
        console.log(this.forms[0].Email);
        console.log(this.forms);  
      })
  }

  onSubject(subj){
    if(subj.valid){
      console.log(subj.viewModel);
    }
    this.forms?.push(subj.viewModel);  
  }

  
  // onSubmit(f : any[]){
  //   this.forms?.push(f);
  
  // }

}
