import { Component } from '@angular/core';
import {ServerService} from './Services/server.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'teacherLandAngular';
  users = [
      {firstName: 'Ali' , lastName: 'Bohlooli' , Age: 22 , rule: 'fullAdmin' },
      {firstName: 'Hadi' , lastName: 'Moradi' , Age: 22 , rule: 'admin' },
      {firstName: 'Ehsan' , lastName: 'Sajjadi' , Age: 21 , rule: 'admin' }
      ];
  constructor(private serverService: ServerService) {}

  onSave = () => {
   this.serverService.storeAdmins(this.users)
       .subscribe(respose => console.log(respose),
               error => console.log(error)
       );
  }
}
