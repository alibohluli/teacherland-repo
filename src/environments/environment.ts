// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false ,
  firebase : {
    apiKey: "AIzaSyApVZ1MNlSn5PUb-QUb3Vn1sc5PeWmhel8",
    authDomain: "angulardemo-b334f.firebaseapp.com",
    databaseURL: "https://angulardemo-b334f-default-rtdb.firebaseio.com",
    projectId: "angulardemo-b334f",
    storageBucket: "angulardemo-b334f.appspot.com",
    messagingSenderId: "68423524747",
    appId: "1:68423524747:web:6813c4c3fe2f689c308f8e"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
